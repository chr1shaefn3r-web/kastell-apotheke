---
title: Serviceleistungen
seoTitle: Serviceleistungen | Kastell-Apotheke Osterburken
author: Kastell-Apotheke
image: der_bringts.jpg
layout: page
language: de
en: /services/
priority: 0.4
pre: anfahrt
weight: 5
---
<div class="row">
	<div class="col-md-6">
	  <h2>Angebote</h2>
	  <ul>
		<li>Anmessen von Kompressionsstrümpfen</li>
		<li>Anmessen von medizinischen Bandagen</li>
	  </ul>
	</div>
	<div class="col-md-6">
	  <h2>Beratungen</h2>
	  <ul>
		<li>Inkontinenzberatung</li>
		<li>Reisemedizinische Beratung</li>
		<li>Überprüfen von Hausapotheken und Verbandskasten</li>
		<li>Beratung von Asthmatikern</li>
		<li>Beratung von Diabetikern</li>
		<li>Beratung über medizinische Hautpflege</li>
	  </ul>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-6">
	  <h2>Untersuchungen</h2>
	  <ul>
		<li>Blutdruckmessung</li>
		<li>Gewicht, Körpergröße, BMI</li>
	  </ul>
	</div>
	<div class="col-md-6">
	  <h2>Verleih</h2>
	  <ul>
		<li><a href="http://www.medela.de/">Milchpumpen</a></li>
		<li>Babywaagen</li>
		<li><a href="http://www.uebe.com/">Blutdruckmessgeräte</a></li>
		<li>Blutzuckermessgeräte</li>
	  </ul>
	</div>
</div>

<div class="row">
	<div class="col-md-offset-3 col-md-6">
	  <img class="kastell-apotheke-bild" alt="Lieferservice der Kastell-Apotheke" src="{{ page.image | prepend: "/assets/" }}" />
	  <div class="derBringts">
		„… der bringt`s“
	  </div>
	</div>
</div>

