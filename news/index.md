---
title: News
seoTitle: News | Kastell-Apotheke Osterburken
description: "Everything going on at the Kastell-Apotheke Osterburken. "
layout: page
language: en
de: /neuigkeiten/
weight: 2
---
<div class="row">
{% for post in site.posts %}
{% if post.language == "en" %}
<div class="col-md-12 blogpost">
	<div class="post-title">
	  <h2><a href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a></h2>
	</div>
	<div class="post-meta">Posted on
		{{ post.date | date: "%d. %B %Y" }}
	</div>
	<div class="row">
		<div class="col-md-3">
			<img src="{{ post.image | prepend: "/assets/" }}" alt="{{ post.title }}" class="img-responsive center-block">
		</div>
		<div class="col-md-9 post-excerpt">
			{% if post.excerpt %}{{ post.content | strip_html | strip_newlines | truncate: 210 }}{% endif %}"
		</div>
	</div>
	<div class="post-readmore">
		<a href="{{ post.url | prepend: site.baseurl }}">Read more&nbsp;›</a>
	</div>
</div>
{% endif %}
{% endfor %}
</div>

<p class="rss-subscribe">subscribe <a href="{{ "/feed.xml" | prepend: site.baseurl }}">via RSS</a> or <a href="{{ "/atom.xml" | prepend: site.baseurl }}">via Atom</a></p>
