# Speed-Analyzation
## https://developers.google.com/speed/pagespeed/insights/
https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fkastell-apotheke.de
 * Run: 03.08.2018
	* Mobile:  100%
	* Desktop:  95%
 * Run: 20.08.2016
	* Mobile:  100%
	* Desktop: 100%
 * Run: 10.01.2016
	* Mobile:  100%
	* Desktop:  97%

## http://www.webpagetest.org
Default Settings and 3G, always First View, https and Frankfurt, Germany (click on Screenshot to get the numbers)
 * Run: 03.08.2018
	* Speed Index: 2088
	* First Byte: 1.6s
	* Start Render: 1.7s
	* Visualy Complete: 2.7s
	* Requests: 30
	* Bytes In: 82kb
 * Run: 20.08.2016
	* Speed Index: 3353
	* First Byte: 2s
	* Start Render: 2.783s
	* Visualy Complete: 3.7s
	* Requests: 20
	* Bytes In: 633kb
 * Run: 10.01.2016
	* Speed Index: 1024;3359
	* First Byte: 0.347s;2.264s
	* Start Render: 0.876s;2.670s
	* Visualy Complete: 1.100s;4.300s
	* Requests: 9;9
	* Bytes In: 626KB;626KB

## http://gtmetrix.com
 * Run: 03.08.2018
	* Page Speed Grade: 100%
	* YSlow Grade: 95%
	* Page load time: 2.5s
	* Total page size: 65.3KB
	* Total number of requests: 10
	* Timeline: 10 Requests 55.9 KB 2.53s (onload: 2.06s)
 * Run: 20.08.2016
	* Page Speed Grade: 94%
	* YSlow Grade: 96%
	* Page load time: 2.6s
	* Total page size: 45.5KB
	* Total number of requests: 7
	* Timeline: 7 Requests 45.5 KB 2.62s (onload: 2.63s)
 * Run: 10.01.2016
	* Page Speed Grade: 94%
	* YSlow Grade: 96%
	* Page load time: 1.8s
	* Total page size: 44.5KB
	* Total number of requests: 7
	* Timeline: 7 Requests 44.5 KB 1.64s (onload: 1.8s)
