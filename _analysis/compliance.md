# Compliance-Analyzation
## https://yellowlab.tools/result/f3igaq1t4i
 * Run: 03.08.2018 => 98%
 * Run: 10.01.2016 => 89%
## https://validator.w3.org/nu/?doc=https%3A%2F%2Fkastell-apotheke.de%2F
 * Run: 03.08.2018 =>  90%
    - Warning: The type attribute for the style element is not needed and should be omitted.
      From line 2, column 265; to line 2, column 287
    - Warning: The type attribute is unnecessary for JavaScript resources.
      From line 6, column 6295; to line 6, column 6325
 * Run: 10.01.2016 => 100% (no errors or warnings to show)

