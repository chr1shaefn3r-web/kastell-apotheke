# SEO-Analyzation
## http://www.site-analyzer.com/
http://www.site-analyzer.com/en/audit/http://kastell-apotheke.de
 * Run: 10.01.2016 => 63.7%
 * Run: 16.10.2015 => 51.7%
 * Run: 18.06.2015 => 78,16%
	* Inlining flag-image with data:image caused one "unreachable image" error
	* Adding html microdata dropped text/code ratio to warning level
 * Run: 08.06.2015 => 81,97%
	* Fixed: www to no-www redirect
 * Run: 02.06.2015 => 78.48%
	* Fixed: h1-Title on index.html (Added "Osterburken")
	* Fixed: More Text on index.html (Added two sentences from My Business-Entry)
 * Run: 28.05.2015 => 75.34%
	* Fixed: title
	* Fixed: keywords
	* Fixed: lang-attr in html-tag
	* Fixed: missing <header>- and <footer>-tag
 * Run: 26.05.2015 => 72.08%

## http://seositecheckup.com
 * Run: 03.08.2018 => 87%
 * Run: 10.01.2016 => 81%
 * Run: 16.10.2015 => 80%
 * Run: 08.06.2015 => 85%
	* Fixed: www to no-www redirect
	* Fixed: replaced "\_" with "-" in image urls
	* Fixed: Added schema.org Metadata to frontpages
 * Run: 04.06.2015 => 77%

