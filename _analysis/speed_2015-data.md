# Speed-Analyzation
## https://developers.google.com/speed/pagespeed/insights/
https://developers.google.com/speed/pagespeed/insights/?url=http%3A%2F%2Fkastell-apotheke.de
 * Run: 16.10.2015
	* Mobile: 100%
	* Desktop: 97%
 * Run: 02.07.2015
	* Mobile: 100%
	* Desktop: 97%
 * Run: 23.06.2015
	* Mobile: 91%
	* Desktop: 95%
 * Run: 18.06.2015
	* Mobile: 91%
	* Desktop: 93%
 * Run: 12.06.2015
	* Mobile: 91%
	* Desktop: 97%
 * Run: 11.06.2015
	* Mobile: 85%
	* Desktop: 95%

## http://www.webpagetest.org
 * Run: 16.10.2015
	* Load Time: 1.184s
	* First Byte: 0.678s
	* Start Render: 1.375s
	* Fully Loaded Time: 1.184s
 * Run: 02.07.2015
	* Load Time: 0.669s
	* First Byte: 0.112s
	* Start Render: 0.586s
	* Fully Loaded Time: 0.669s
 * Run: 18.06.2015
	* Load Time: 1.621s
	* First Byte: 0.427s
	* Start Render: 0.995s
	* Fully Loaded Time: 1.913s
 * Run: 12.06.2015
	* Load Time: 1.552s
	* First Byte: 0.297s
	* Start Render: 0.593s
	* Fully Loaded Time: 1.680s
 * Run: 11.06.2015
	* Load Time: 3.250s
	* First Byte: 0.541s
	* Start Render: 1.394s
	* Fully Loaded Time: 3.379s

## http://gtmetrix.com
 * Run: 16.10.2015
	* Page Speed Grade: 96%
	* YSlow Grade: 95%
	* Page load time: 3.10s
	* Total page size: 60.8KB
	* Total number of requests: 8
	* Timeline: 8 Requests 60.8 KB 2.97s (onload: 3.15s)
 * Run: 02.07.2015
	* Page Speed Grade: 98%
	* YSlow Grade: 96%
	* Page load time: 2.10s
	* Total page size: 42.4KB
	* Total number of requests: 7
	* Timeline: 7 Requests 42.4 KB 1.84s (onload: 2.11s)
 * Run: 18.06.2015
	* Page Speed Grade: 99%
	* YSlow Grade: 96%
	* Page load time: 2.26s
	* Total page size: 40.3KB
	* Total number of requests: 7
	* Timeline: 7 Requests 40.3 KB 2.09s (onload: 2.27s)
 * Run: 12.06.2015
	* Page Speed Grade: 99%
	* YSlow Grade: 95%
	* Page load time: 2.70s
	* Total page size: 40.2KB
	* Total number of requests: 9
	* Timeline: 9 Requests 40.2 KB 2.4s (onload: 2.7s)
 * Run: 11.06.2015
	* Page Speed Grade: 99%
	* YSlow Grade: 94%
	* Page load time: 3.13s
	* Total page size: 57.6KB
	* Total number of requests: 10
	* Timeline: 10 Requests 57.6 KB 2.87s (onload: 3.14s)

