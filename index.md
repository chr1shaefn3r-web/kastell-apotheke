---
title: Kastell-Apotheke
seoTitle: Die Kastell-Apotheke Osterburken
h1Title: Kastell-Apotheke Osterburken
author: Kastell-Apotheke
description: "Die Startseite der Kastell-Apotheke Osterburken."
layout: page
language: de
en: /en/
priority: 1
pre: neuigkeiten
weight: 1
---
<div class="row">
	<div class="col-sm-6">

		<h2>Ursula Häfner e. Kfr.</h2>

		<h3>Fachapothekerin für Offizinpharmazie</h3>

		<div itemscope itemtype="http://schema.org/LocalBusiness">
			<span class="hidden" itemprop="name">Kastell-Apotheke</span>
			<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<p>
					<span itemprop="streetAddress">Prof.-Schumacher-Str. 2/8</span><br>
					<span itemprop="postalCode">74706</span> <span itemprop="addressLocality">Osterburken</span><span class="hidden" itemprop="addressRegion">Baden-Württemberg</span>
				</p>
			</div>
			<ul>
			  <li>Telefon: <span itemprop="telephone">06291&nbsp;/&nbsp;68007</span></li>
			  <li>Telefax: <span itemprop="faxNumber">06291&nbsp;/&nbsp;68008</span></li>
			  <li>Kostenlose Bestellnummer: 0800&nbsp;/&nbsp;6800700<br>
					Bestell-eMail: <a href="mailto:bestellungen@kastell-apotheke.de">bestellungen@kastell-apotheke.de</a></li>
			  <li>Kontakt-eMail: <a href="mailto:info@kastell-apotheke.de">info@kastell-apotheke.de</a></li>
			</ul>
			<a class="hidden" itemprop="url" href="https://kastell-apotheke.de/">Home</a>
		</div>
	</div>
	<div class="col-sm-6">
		<img class="img-responsive center-block kastell-apotheke-bild" alt="Eingang zur Kastell-Apotheke Osterburken" src="/assets/kastell-apotheke-osterburken.jpg" />
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="main-announcements-title">Wir suchen Mitarbeiter</div>
		<ul>
			<li><a href="/stellen">Pharmazeutisch-technischer Assistent (m/w/d)</a></li>
			<li><a href="/stellen">Pharmazeutisch-kaufmännischer Assistent (m/w/d)</a></li>
		</ul>
	</div>
	<div class="col-md-12">

{% if site.data.announcements %}
<div class="main-announcements important">
<div class="main-announcements-title">Aktuelles</div>
{% for a in site.data.announcements %}
<div class="main-announcement important">
	{% if a.dei && a.deil %}
		<a href="{{ a.deil | prepend: "/assets/" }}"><img class="kastell-apotheke-bild ankuendigungen-bild" alt="{{ a.deia }}" src="{{ a.dei | prepend: "/assets/" }}" /></a>
	{% endif %}
	<div class="main-announcement-header"><span class="main-announcement-title">{{ a.de }}</span>{% if a.ded %} <span class="announcement-date">({{ a.ded }})</span>{% endif %}</div>
	<div class="main-announcement-snippet">{{ a.del }}</div>
</div>
{% endfor %}
</div>
{% endif %}

		<table>
			<caption>Die täglichen Öffnungszeiten der Kastell-Apotheke Osterburken.</caption>
			<tr>
				<th colspan="2">Öffnungszeiten</th>
			</tr>

			<tr>
				<td>Mo. &#8211; Sa.:</td>
				<td>08:00 &#8211; 13:00</td>
			</tr>

			<tr>
				<td>Mo. &#8211; Fr.:</td>
				<td>14:00 &#8211; 19:00</td>
			</tr>

		</table>

		<p>
			Samstag-Nachmittag geschlossen<br>
			<a href="http://www.lak-bw.notdienst-portal.de/?suchbegriff=74706" rel="nofollow">Notdienst</a> nach Dienstplan
		</p>

		<p>
			Die Kastell-Apotheke in Osterburken, Ihre Apotheke vor Ort. Der kompetente Ansprechpartner in allen Gesundheitsfragen.
		</p>
	</div>
</div>
