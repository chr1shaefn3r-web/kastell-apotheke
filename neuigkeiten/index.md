---
title: Neuigkeiten
seoTitle: Neuigkeiten | Kastell-Apotheke Osterburken
description: "Alle Neuigkeiten rund um die Kastell-Apotheke Osterburken."
layout: page
language: de
en: /news/
priority: 0.8
pre: mitarbeiter
weight: 2
---
<div class="row">
{% for post in site.posts %}
{% if post.language == "de" %}
<div class="col-md-12 blogpost">
	<div class="post-title">
	  <h2><a href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a></h2>
	</div>
	<div class="post-meta">Veröffentlicht am
		{% assign m = post.date | date: "%-m" %}
		{{ post.date | date: "%-d." }}
		{% case m %}
		  {% when '1' %}Januar
		  {% when '2' %}Februar
		  {% when '3' %}M&auml;rz
		  {% when '4' %}April
		  {% when '5' %}Mai
		  {% when '6' %}Juni
		  {% when '7' %}Juli
		  {% when '8' %}August
		  {% when '9' %}September
		  {% when '10' %}Oktober
		  {% when '11' %}November
		  {% when '12' %}Dezember
		{% endcase %}
		{{ post.date | date: "%Y" }}
	</div>
	<div class="row">
		<div class="col-md-3">
			<img src="{{ post.image | prepend: "/assets/" }}" alt="{{ post.title }}" class="img-responsive center-block">
		</div>
		<div class="col-md-9 post-excerpt">
			{% if post.excerpt %}{{ post.content | strip_html | strip_newlines | truncate: 210 }}{% endif %}"
		</div>
	</div>
	<div class="post-readmore">
		<a href="{{ post.url | prepend: site.baseurl }}">Weiterlesen&nbsp;›</a>
	</div>
</div>
{% endif %}
{% endfor %}
</div>

<p class="rss-subscribe">abonnieren <a href="{{ "/feed.xml" | prepend: site.baseurl }}">via RSS</a> oder <a href="{{ "/atom.xml" | prepend: site.baseurl }}">via Atom</a></p>
