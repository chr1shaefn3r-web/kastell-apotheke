#!/usr/bin/env bash

for f in _build/css/*.css; do
	echo "> Minify $f";
	cleancss -d -o $f $f
done

