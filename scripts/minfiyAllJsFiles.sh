#!/usr/bin/env bash

for f in _build/js/*.js; do
	echo "> Minify $f";
	uglifyjs -c -o $f $f
done

