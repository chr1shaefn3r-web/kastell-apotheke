# $0 - first parameter = always the script itself
# $1 - expect the file
# $2 - expect the minimum-size
file=$1
minimumsize=$2
actualsize=$(wc -c <"$file")
if [ $actualsize -ge $minimumsize ]; then
	# everything seams fine
	exit 0;
else
	# file looks to small
	exit 1;
fi
