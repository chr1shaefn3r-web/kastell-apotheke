#!/usr/bin/env bash

echo "> Minify _build/index.html:";
node-minify --compressor html-minifier --input _build/index.html --output _build/index.html
for f in _build/**/*.html; do
	echo "> Minify $f";
	node-minify --compressor html-minifier --input "$f" --output "$f"
done

