---
title: Stellenangebote
seoTitle: Stellenangebote | Kastell-Apotheke Osterburken
author: Kastell-Apotheke
image: Anfahrt.png
description: "Die Kastell-Apotheke sucht tatkräftige Unterstützung."
layout: page
language: de
priority: 0.6
weight: 4
---

Bei Interesse melden Sie sich gerne unkompliziert per eMail ([info@kastell-apotheke.de](mailto:info@kastell-apotheke.de)), Telefon (06291/68007) oder persönlich.

## Pharmazeutisch-technischer Assistent (m/w/d)

**Beginn:** Ab sofort
**Arbeitsumfang:** ab 15 Std. pro Woche

Wir suchen SIE, eine/n engagierte/n PTA mit Freude an Rezeptur und Labor - gerne Wiedereinsteiger/innen und Berufsanfänger/innen und 
wir erwarten selbständiges und teamorientiertes Arbeiten und viel Empathie für unsere Kunden.
Wir bieten Ihnen eine intensive Einarbeitung durch Ihre neuen Kolleginnen, Arbeiten mit Rowa, NIR- Spektometer, QMS und 
ein wertschätzendes Umfeld.

## Pharmazeutisch-kaufmännischer Assistent (m/w/d)

**Beginn:** Ab sofort
**Arbeitsumfang:** ab 20 Std. pro Woche

Wir suchen SIE, eine/n engagierte/n PKA mit Freude an einem perfekten Backoffice.
Wir erwarten exaktes Arbeiten, Organisationstalent und viel Empathie für unsere Kunden am Telefon. 
Sie arbeiten mit PT IXOS, einem Rowa und digitalen Anwendungen.
Sie erhalten eine fundierte Einarbeitung durch Ihre neuen Kolleginnen und arbeiten in einem sympatischen Team mit flexiblen Arbeitszeiten   
