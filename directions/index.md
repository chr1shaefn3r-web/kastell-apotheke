---
title: Directions
seoTitle: Directions | Kastell-Apotheke Osterburken
author: Kastell-Apotheke
image: Anfahrt.jpg
description: "How to get to the Kastell-Apotheke Osterburken. "
layout: page
language: en
de: /anfahrt/
weight: 5
---
## Address

Kastell-Apotheke  
Prof.-Schumacher-Str. 2/8  
D-74706 Osterburken

## &#8230; and thats how you find us:

<a href="https://www.openstreetmap.org/?mlat=49.4276&mlon=9.4219#map=16/49.4276/9.4219" rel="nofollow" target="_blank"><img src="{{ page.image | prepend: "/assets/" }}" alt="Karte zur Anfahrt an die Kastell-Apotheke in Osterburken" class="kastell-apotheke-bild img-responsive center-block" /></a>

<p>
	<a href="https://www.openstreetmap.org/?mlat=49.4276&mlon=9.4219#map=16/49.4276/9.4219" rel="nofollow" target="_blank">Show a bigger map</a>, © <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a> contributors
</p>
