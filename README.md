# Kastell-Apotheke

Homepage of the Kastell-Apotheke, hosted at [kastell-apotheke.de](https://kastell-apotheke.de/).

## Upgrade dependencies

`npx npm-check-updates --enginesNode --packageManager npm --doctor -u`

## Pending version upgrades

 * css-purge to v3: error: unknown option `--no-watch'
 * workbox-cli to v6: Unable to find a place to inject the manifest. Please ensure that your service worker file contains the following: self.__WB_MANIFEST
## Versionshistory

### 3.0
**Technology**: Jekyll

After hearing about Wordpress security vulnerabilities nearly every week, amplified by security vulnerabilities in plugins, Christoph made the switch to Jekyll and static content.

### 2.0
**Technology**: Wordpress

Shortly after the acquisition of the pharmacy, Christoph updated the website powered by [Wordpress](https://wordpress.com/).

### 1.0
**Technology**: Flash

The version of the previous owner of the pharmacy, based on Flash.

