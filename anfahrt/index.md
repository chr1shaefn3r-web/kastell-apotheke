---
title: Anfahrt
seoTitle: Anfahrt | Kastell-Apotheke Osterburken
author: Kastell-Apotheke
image: Anfahrt.jpg
description: "Alle Informationen dazu wie die Kastell-Apotheke in Osterburken zu finden ist. "
layout: page
language: de
en: /directions/
priority: 0.4
weight: 6
---
## Anschrift

Kastell-Apotheke  
Prof.-Schumacher-Str. 2/8  
74706 Osterburken

## &#8230; und so finden Sie uns: 
<a href="https://www.openstreetmap.org/?mlat=49.4276&mlon=9.4219#map=16/49.4276/9.4219" rel="nofollow" target="_blank"><img src="{{ page.image | prepend: "/assets/" }}" alt="Karte zur Anfahrt an die Kastell-Apotheke in Osterburken" class="kastell-apotheke-bild img-responsive center-block" /></a>

<p>
	<a href="https://www.openstreetmap.org/?mlat=49.4276&mlon=9.4219#map=16/49.4276/9.4219" rel="nofollow" target="_blank">Gr&ouml;ßere Karte anzeigen</a>, © <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a> contributors
</p>
