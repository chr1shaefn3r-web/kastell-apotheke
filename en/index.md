---
title: Kastell-Apotheke
seoTitle: The Kastell-Apotheke Osterburken
h1Title: Kastell-Apotheke Osterburken
author: Kastell-Apotheke
description: "Homepage of the Kastell-Apotheke Osterburken."
layout: page
language: en
de: /
priority: 1
weight: 1
---
<div class="row">
	<div class="col-sm-6">

		<h2>Ursula Häfner e. Kfr.</h2>

		<h3>Specialist in community pharmacy</h3>

		<div itemscope itemtype="http://schema.org/LocalBusiness">
			<span class="hidden" itemprop="name">Kastell-Apotheke</span>
			<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<p>
					<span itemprop="streetAddress">Prof.-Schumacher-Str. 2/8</span><br>
					<span itemprop="postalCode">D-74706</span> <span itemprop="addressLocality">Osterburken</span><span class="hidden" itemprop="addressRegion">Baden-Württemberg</span>
				</p>
			</div>
			<ul>
			  <li>Phone: <span itemprop="telephone">+49 6291&nbsp;/&nbsp;68007</span></li>
			  <li>Fax: <span itemprop="faxNumber">+49 6291&nbsp;/&nbsp;68008</span></li>
			  <li>Free ordering hotline: 0800&nbsp;/&nbsp;6800700<br>
					ordering-eMail: <a href="mailto:bestellungen@kastell-apotheke.de">bestellungen@kastell-apotheke.de</a></li>
			  <li>Contact-eMail: <a href="mailto:info@kastell-apotheke.de">info@kastell-apotheke.de</a></li>
			</ul>
			<a class="hidden" itemprop="url" href="https://kastell-apotheke.de/">Home</a>
		</div>
	</div>
	<div class="col-sm-6">
		<img class="img-responsive center-block kastell-apotheke-bild" alt="Entrance to the Kastell-Apotheke Osterburken" src="/assets/kastell-apotheke-osterburken.jpg"/>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table>
			<caption>The daily opening-hours of the Kastell-Apotheke in Osterburken.</caption>
			<tr>
				<th colspan="2">Opening-Hours</th>
			</tr>

			<tr>
				<td>Mo. &#8211; Sa.:</td>
				<td>08:00 &#8211; 13:00</td>
			</tr>

			<tr>
				<td>Mo. &#8211; Fr.:</td>
				<td>14:00 &#8211; 19:00</td>
			</tr>
		</table>

		<p>
			Closed on Saturday afternoon.<br>
			<a href="http://www.lak-bw.notdienst-portal.de/?suchbegriff=74706" rel="nofollow">Emergency Service</a> according to the schedule.
		</p>

	</div>
</div>
