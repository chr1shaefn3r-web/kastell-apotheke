# BEGIN ErrorDocuments
ErrorDocument 404 /404.html
# END ErrorDocuments

# Make Apache sent out Content-type with
# the correct charset
AddDefaultCharset utf-8

# BEGIN Compression
<ifModule mod_gzip.c>
    mod_gzip_on Yes
    mod_gzip_dechunk Yes
    mod_gzip_item_include file .(html?|txt|css|js|php|pl)$
    mod_gzip_item_include handler ^cgi-script$
    mod_gzip_item_include mime ^text/.*
    mod_gzip_item_include mime ^application/x-javascript.*
    mod_gzip_item_exclude mime ^image/.*
    mod_gzip_item_exclude rspheader ^Content-Encoding:.*gzip.*
</ifModule>
# compress text, html, javascript, css, xml:
AddOutputFilterByType DEFLATE text/plain
AddOutputFilterByType DEFLATE text/html
AddOutputFilterByType DEFLATE text/xml
AddOutputFilterByType DEFLATE text/css
AddOutputFilterByType DEFLATE application/xml
AddOutputFilterByType DEFLATE application/xhtml+xml
AddOutputFilterByType DEFLATE application/rss+xml
AddOutputFilterByType DEFLATE application/javascript
AddOutputFilterByType DEFLATE application/x-javascript
# END Compression

# BEGIN CACHING
<IfModule mod_expires.c>
    ExpiresActive On
    ExpiresByType image/jpg "access 1 year"
    ExpiresByType image/jpeg "access 1 year"
    ExpiresByType image/gif "access 1 year"
    ExpiresByType image/png "access 1 year"
    ExpiresByType application/pdf "access 1 month"
    ExpiresByType application/javascript "access 1 week"
    ExpiresByType text/x-javascript "access 1 week"
    ExpiresByType text/javascript "access 1 week"
    ExpiresByType text/js "access 1 week"
    ExpiresByType image/x-icon "access 1 year"
    ExpiresDefault "access 2 days"
    <filesMatch "\.(js)$">
	ExpiresDefault "access 1 week"
    </filesMatch>
	# Don't cache html-sites due to important announcements!
	<filesMatch "\.(html|htm)$">
	  <ifModule mod_headers.c>
		 Header set Cache-Control "max-age=0, no-cache, no-store, must-revalidate"
		 Header set Pragma "no-cache"
	  </ifModule>
	</filesMatch>
	# Don't cache the service worker file
	<filesMatch "sw\.js$">
	  <ifModule mod_headers.c>
		 Header set Cache-Control "max-age=0, no-cache, no-store, must-revalidate"
		 Header set Pragma "no-cache"
	  </ifModule>
	</filesMatch>
</IfModule>
# END CACHING

# BEGIN SECURITY
ServerSignature Off
<IfModule mod_headers.c>
	Header set X-Frame-Options "SAMEORIGIN"
	Header set Content-Security-Policy "default-src 'self'; base-uri 'self'; manifest-src 'self'; font-src 'self'; prefetch-src 'self'; script-src 'self' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; img-src 'self' data:; frame-src 'self'; frame-ancestors 'self'; form-action 'none'; child-src 'self'; upgrade-insecure-requests; block-all-mixed-content;"
	Header always set Strict-Transport-Security "max-age=16070400; includeSubDomains"
	Header set X-Content-Type-Options "nosniff"
	Header set X-XSS-Protection "1; mode=block"
	# `mod_headers` cannot match based on the content-type, however,
	# the `X-Frame-Options` response header should be send only for
	# HTML documents and not for the other resources.
	<FilesMatch "\.(appcache|atom|bbaw|bmp|crx|css|cur|eot|f4[abpv]|flv|geojson|gif|htc|ico|jpe?g|js|json(ld)?|m4[av]|manifest|map|mp4|oex|og[agv]|opus|otf|pdf|png|rdf|rss|safariextz|svgz?|swf|topojson|tt[cf]|txt|vcard|vcf|vtt|webapp|web[mp]|webmanifest|woff2?|xloc|xml|xpi)$">
		Header unset X-Frame-Options
		Header unset Content-Security-Policy
		Header unset X-XSS-Protection
	</FilesMatch>
	Header unset X-Powered-By
	Header set Referrer-Policy "same-origin"
</IfModule>
# END SECURITY

# BEGIN SEO-Rewriting
<IfModule mod_rewrite.c>
	RewriteEngine On
	# Redirect http://www to non-www
	RewriteCond %{SERVER_PORT} ^80$
	RewriteCond %{HTTP_HOST} ^www\.kastell-apotheke\.de$ [NC]
	RewriteRule ^(.*)$ http://kastell-apotheke.de/$1 [L,R=301]
	# Redirect https://www to non-www
	RewriteCond %{SERVER_PORT} ^443$
	RewriteCond %{HTTP_HOST} ^www\.kastell-apotheke\.de$ [NC]
	RewriteRule ^(.*)$ https://kastell-apotheke.de/$1 [L,R=301]
    Redirect 301 "/willkommen.html" "/index.html"
    Redirect 301 "/impressum.html" "/impressum/"
    Redirect 301 "/mitarbeiter.html" "/mitarbeiter/"
    Redirect 301 "/kontakt.html" "/index.html"
    Redirect 301 "/service.html" "/serviceleistungen/"
    Redirect 301 "/author/root" "/neuigkeiten/"
    Redirect 301 "/author/uhaefner/" "/neuigkeiten/"
    Redirect 301 "/ankuendigungen/" "/neuigkeiten/"
    Redirect 301 "/2013/10/21/ankuendigungenblutdruckmessgeraete-prueftag/" "/neuigkeiten/"
    Redirect 301 "/2013/10/21/blutdruckmessgeraete-prueftag/" "/neuigkeiten/"
    Redirect 301 "/en/author/root/" "/news/"
    Redirect 301 "/en/author/kastell-apotheke/" "/news/"
    Redirect 301 "/index.htmlauthor/root/" "/neuigkeiten/"
    Redirect 301 "/index.htmlauthor/uhaefner/" "/neuigkeiten/"
    Redirect 301 "/de/" "/index.html"
    Redirect 301 "/infos/" "/index.html"
    Redirect 301 "/infos/anfahrt/" "/anfahrt/"
    Redirect 301 "/infos/pollenflug/" "/index.html"
    Redirect 301 "/index.htmlpollenflug/" "/index.html"
    Redirect 301 "/information/" "/index.html"
    Redirect 301 "/information/pollination/" "/index.html"
    Redirect 301 "/index.htmlpollination/" "/index.html"
    Redirect 301 "/emergency-service/" "/index.html"
    Redirect 301 "/en/emergency-service/" "/index.html"
    Redirect 301 "/en/employees/" "/employees/"
    Redirect 301 "/notdienst/" "/index.html"
    Redirect 301 "/notdienst_1HJ.pdf" "/index.html"
    Redirect 301 "/notdienst_2HJ.pdf" "/index.html"
    Redirect 301 "/index.htmldirections/" "/directions/"
    Redirect 301 "/index.htmldirection/" "/directions/"
    Redirect 301 "/index.htmlanfahrt/" "/anfahrt/"
    Redirect 301 "/xmlrpc.php" "/index.html"
    Redirect 301 "/k1.swf" "/index.html"
    Redirect 301 "/k1out.swf" "/index.html"
    Redirect 301 "/k2.swf" "/index.html"
    Redirect 301 "/k6.swf" "/index.html"
    Redirect 301 "/k7out.swf" "/index.html"
    Redirect 301 "/k8.swf" "/index.html"
    Redirect 301 "/k9.swf" "/index.html"
    Redirect 301 "/k3out.swf" "/index.html"
    Redirect 301 "/l.swf" "/index.html"
    Redirect 301 "/$1" "/index.html"
    Redirect 301 "/menu.html" "/index.html"
    Redirect 301 "/down.html" "/index.html"
    Redirect 301 "/empty1.html" "/index.html"
    Redirect 301 "/sitemap-misc.html" "/sitemap.xml.gz"
    Redirect 301 "/sitemap-pt-post-2013-06.html" "/sitemap.xml.gz"
    Redirect 301 "/2013/07/08/venen-messwoche/" "/2013/07/08/venen-messwoche.html"
    Redirect 301 "/2013/07/08/test-your-vein-week/" "/2013/07/08/test-your-vein-week.html"
    Redirect 301 "/2013/07/23/zertifikat-verliehen/" "/2013/07/23/zertifikat-verliehen.html"
    Redirect 301 "/2013/07/23/certificate-awarded/" "/2013/07/23/certificate-awarded.html"
    Redirect 301 "/2013/06/17/neue-homepage-online/" "/2013/06/17/neue-homepage-online.html"
    Redirect 301 "/2013/06/17/new-homepage-online/" "/2013/06/17/new-homepage-online.html"
    Redirect 301 "/en/2013/06/17/new-homepage-online/" "/2013/06/17/new-homepage-online.html"
    Redirect 301 "/2013/07/08/venen-messwoche/" "/2013/07/08/venen-messwoche.html"
    Redirect 301 "/2013/10/14/borkemer-herbst/" "/2013/10/14/borkemer-herbst.html"
    Redirect 301 "/images/ulla.jpg" "/assets/employees/haefner_ulla.jpg"
    Redirect 301 "/images/brenner.jpg" "/assets/employees/brenner_dorothea.jpg"
    Redirect 301 "/images/stahl.jpg" "/assets/employees/stahl_gudrun.jpg"
    Redirect 301 "/images/asselborn.jpg" "/assets/employees/asselborn_rita.jpg"
    Redirect 301 "/images/gramlich.jpg" "/assets/employees/gramlich_doris.jpg"
    Redirect 301 "/images/moll.jpg" "/assets/employees/moll_ingrid.jpg"
    Redirect 301 "/images/hph.jpg" "/assets/employees/haefner_hans-peter.jpg"
    Redirect 301 "/assets/haefner_ulla.jpg" "/assets/employees/haefner_ulla.jpg"
    Redirect 301 "/assets/brenner_dorothea.jpg" "/assets/employees/brenner_dorothea.jpg"
    Redirect 301 "/assets/stahl_gudrun.jpg" "/assets/employees/stahl_gudrun.jpg"
    Redirect 301 "/assets/asselborn_rita.jpg" "/assets/employees/asselborn_rita.jpg"
    Redirect 301 "/assets/gramlich_doris.jpg" "/assets/employees/gramlich_doris.jpg"
    Redirect 301 "/assets/moll_ingrid.jpg" "/assets/employees/moll_ingrid.jpg"
    Redirect 301 "/assets/haefner_hans-peter.jpg" "/assets/employees/haefner_hans-peter.jpg"
    Redirect 301 "/kastell-apotheke-2/" "/index.html"
    RedirectMatch 301 ^/wp-content/.*$ "/index.html"
</IfModule>
# END SEO-Rewriting

