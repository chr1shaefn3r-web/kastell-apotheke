---
title: Services
seoTitle: Services | Kastell-Apotheke Osterburken
author: Kastell-Apotheke
image: der_bringts.jpg
layout: page
language: en
de: /serviceleistungen/
weight: 4
---
<div class="row">
	<div class="col-md-6">
	  <h2>Offerings</h2>
	  <ul>
		<li>Measure for compression hosiery</li>
		<li>Measure for medical bandages</li>
	  </ul>
	</div>
	<div class="col-md-6">
	  <h2>Consulting</h2>
	  <ul>
		<li>Incontinence</li>
		<li>&#8230;</li>
	  </ul>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-6">
	  <h2>Examination</h2>
	  <ul>
		<li>blut pressure reading</li>
		<li>&#8230;</li>
	  </ul>
	</div>
	<div class="col-md-6">
	  <h2>Rental</h2>
	  <ul>
		<li><a href="http://www.medela.de/">breast-pumps</a></li>
		<li>&#8230;</li>
	  </ul>
	</div>
</div>

<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<img class="kastell-apotheke-bild" alt="Delivery service from the Kastell-Apotheke in Osterburken" src="{{ page.image | prepend: "/assets/" }}" />
		<div class="derBringts">
			„… delivered free“
		</div>
	</div>
</div>

