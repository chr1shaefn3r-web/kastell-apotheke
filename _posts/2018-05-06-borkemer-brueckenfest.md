---
title: Borkemer Brückenfest
image: "borkemer_brueckenfest_kastell_apotheke_18.jpg"
author: uh
layout: post
language: de
category: Neuigkeiten
---
Am diesjährigen Brückenfest beteiligte sich die Kastell-Apotheke am verkaufsoffenen Sonntag.
Sie unterstütze die Kirchliche Sozialstation bei ihrer Blutzuckermessaktion mit der Spende der Teststreifen.
Den Kunden bot sie an diesem Tag einen Brückenfest-Rabatt von 10 % auf ihren Einkauf aus dem freiverkäuflichen Apothekensortiment.

