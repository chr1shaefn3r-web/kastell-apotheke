---
title: Venen-Messwoche
image: "Venen-Messwoche.png"
author: uh
layout: post
language: de
en: /2013/07/08/test-your-vein-week.html
category: Neuigkeiten
---
Eine ganze Woche lang bietet Ihnen die Kastell-Apotheke die Möglichkeit, kostenlos Ihre Venenfunktion überprüfen zu lassen.

## Terminvergabe

Bitte vereinbaren Sie Ihren persönlichen Termin in der Apotheke oder telefonisch unter 06291/68007. Die Messung und das Beratungsgespräch dauern etwa eine halbe Stunde. Die Venenfunktion wird am Unterschenkel gemessen, daher ist lockere Beinkleidung erwünscht.

## Messmethode

Zum Einsatz kommt bei uns die bewährte Lichtreflexionsrheographie, ein schmerzfreies Verfahren zur Erkennung von Venenerkrankungen mittels Infrarotlicht. Das ist so einfach wie Blutdruck messen!

## Kiliani-Markt

Wir bieten die Messung auch am verkaufsoffenen Sonntag im Rahmen des Kiliani-Marktes an.

Weitere Informationen zu Venenproblemen finden Sie [hier][1].

 [1]: /assets/Venen-Messwoche_13.pdf
