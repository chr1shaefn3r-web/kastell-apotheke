---
title: Wir sind für unsere Kunden da – auch nachts
image: "notdienst_17.jpg"
layout: post
author: mm
language: de
category: Neuigkeiten
---
<div class="kastell-apotheke-bild">
	<img src="/assets/fn-web-logo.png" class="img-responsive align-right twenty-percent-width" alt="FN Web Logo" />
	Dieser Artikel erschien urspr&uuml;nglich in den Fr&auml;nkischen Nachrichten am 18.&nbsp;November&nbsp;2017.
	&copy; Fränkische Nachrichten <a href="https://www.fnweb.de/fraenkische-nachrichten_artikel,-osterburken-wir-sind-fuer-unsere-kunden-da-auch-nachts-_arid,1150161.html">Originalartikel</a>
</div>

Wen in der Nacht plötzlich Durchfall, eine starke Erkältung, Fieber oder Zahnschmerz plagt, der bekommt in der Notdienstapotheke das Medikament, das er zur Linderung seiner Beschwerden dringend benötigt.

Osterburken. Nicht nur nachts, sondern auch jeden Sonn- und Feiertag tun bis zu 170 Apotheken in Baden-Württemberg Dienst und garantieren rund um die Uhr eine flächendeckende Versorgung der Bevölkerung mit Arzneimitteln.

Pro Jahr werden so rund 60 000 Nacht- und Notdienste von den Apotheken im Ländle geleistet. Dies und vieles mehr haben die Fränkischen Nachrichten am Mittwoch von Ulla Häfner erfahren: "Der Notdienst beginnt um 8.30 Uhr und endet um 8.30 Uhr am Folgetag. Unter der Woche fällt das nicht so auf, weil die Apotheken tagsüber ja geöffnet haben", verriet die Besitzerin der Kastell-Apotheke, die am Mittwoch den Apothekennotdienst in Osterburken sicherstellte.

## Über Notdienstglocke erreichbar

Nach dem "regulären" Feierabend schließt Ulla Häfner die Eingangstür zu ihrer Apotheke ab - und ist ab diesem Moment über die Notdienstglocke erreichbar. Egal, ob sie Verwaltungsarbeiten in ihrem Büro im Erdgeschoss erledigt oder schon im Notdienstzimmer im ersten Obergeschoss die Beine hochgelegt hat.

Dessen Einrichtung - eine wohnliche Ausstattung mit Bett, Fernseher und Radio - ist vorgeschrieben. Und wird laut Ulla Häfner auch überwacht: Vom Pharmazierat, der in den Apotheken unangemeldet vorbeikommt, um die Einhaltung zahlreicher Vorschriften und Gesetze zu überprüfen.

## Viele Kunden kommen ohne Rezept

"Über 50 Prozent der Patienten im Notdienst waren zuvor nicht beim Arzt, kommen also ohne Rezept", weiß die Apothekerin aus langjähriger Erfahrung zu berichten. Zwischen 18 und 21 Uhr unter der Woche und an den Wochenenden würde ihr Service am häufigsten in Anspruch genommen. "Da ist schon was los." Aber auch nach Mitternacht gebe es Notfälle: akuter Durchfall, eine starke Erkältung, eine Mittelohrentzündung, Fieber und Schmerzen vor allem bei kleinen Kindern - die dann meist im Auto der aufgelösten Eltern auf dem Rücksitz auf deren Rückkehr und Schmerzlinderung warten - gehören zu so manchem "Einsatz" dazu.

Ulla Häfner ist es wichtig, zu erwähnen, dass "das segensreiche Angebot des Notdienstes" nur von Vor-Ort-Apotheken geleistet wird und nicht von Internetversendern und Drogeriemärkten. Sie bittet die Bevölkerung, "diesen Gedanken bei der Einkaufsentscheidung zu berücksichtigen".

Übrigens: Wer die Dienste einer Notdienstapotheke an Wochentagen zwischen 20 Uhr abends und 6 Uhr morgens und an Sonn- und Feiertage ganztags in Anspruch nimmt, hat neben den Kosten für das Medikament eine sogenannte Notdienstgebühr zu entrichten - diese beträgt 2,50 Euro.

Kostenlos ist hingegen das "offene Ohr" für die Sorgen und Nöte ihrer Kunden, für die die Apothekerin natürlich auch gute Tipps auf Lager hat - nicht nur, was die korrekte Einnahme der Tropfen oder Pillen betrifft. Während sie das dringend benötigte Medikament durch das kleine Türchen reicht, wünscht sie "Gute Besserung".

## Alle elf Tage an der Reihe

Der Notdienstkreis Buchen-Adelsheim, zu dem auch die Kastell-Apotheke von Ulla Häfner gehört, besteht aus 17 Apotheken. Um das gesamte Gebiet angemessen zu versorgen, sind manchmal auch zwei Apotheken gleichzeitig zum Notdienst eingeteilt. "Das führt dazu, dass die Apotheken in dieser Region alle elf Tage Notdienst leisten. Das entspricht einer sehr starken Belastung", heißt es in einer Mitteilung der Landesapothekenkammer Baden-Württemberg (LAK), der auch zu entnehmen ist, dass die Kollegen in städtischen Gebieten "nicht ganz so häufig" dran kommen.

Von sinkenden Apothekenzahlen seien alle Stadt- und Landkreise betroffen. "Im gesamten Neckar-Odenwald-Kreis ist die Apothekenzahl in den vergangenen zehn Jahren um sieben gesunken (heute noch 34)", so die LAK, und weiter: "Schuld daran sind die sich verschlechternden wirtschaftlichen Rahmenbedingungen. Die Politik hat in den vergangenen Jahren das Honorar für Apotheken nur minimal angepasst." Auch die sinkende Arztzahl habe Konsequenzen, da Apotheken wirtschaftlich auf die Rezepte aus den Arztpraxen angewiesen seien.

Bei den Anordnungen des Apothekennotdienstes berücksichtig die LAK Baden-Württemberg einerseits die Gewährleistung einer sicheren Arzneimittelversorgung der Bevölkerung und andererseits die Belastung der Apotheke und deren Mitarbeiter durch den Notdienst, der einen Zeitraum von jeweils 24 Stunden umfasst. Aufgrund der sinkenden Apothekenzahl nimmt die Belastung der Apotheker durch den Notdienst kontinuierlich zu.

<div class="kastell-apotheke-bild">
	<img src="/assets/fn-web-logo.png" class="img-responsive align-right twenty-percent-width" alt="FN Web Logo" />
	Dieser Artikel erschien urspr&uuml;nglich in den Fr&auml;nkischen Nachrichten am 18.&nbsp;November&nbsp;2017.
	&copy; Fränkische Nachrichten <a href="https://www.fnweb.de/fraenkische-nachrichten_artikel,-osterburken-wir-sind-fuer-unsere-kunden-da-auch-nachts-_arid,1150161.html">Originalartikel</a>
</div>

