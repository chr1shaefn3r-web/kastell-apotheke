---
title: Brückenfest - Tag der offenen Tür
image: "brueckentag_tag_der_offenen_tuer_15.jpg"
author: uh
layout: post
language: de
category: Neuigkeiten
---
Die Kastell-Apotheke in Osterburken beteiligte sich am Brückenfestsonntag mit einem Tag der offenen Tür und bot ein vielfältiges Programm.

<div class="row">
	<div class="col-md-12">
		<h2>Führung</h2>
	</div>
	<div class="col-md-8">
		Die interessierte Bevölkerung konnte sich in einer Führung ein Bild über die Funktionsräume einer modernen Apotheke machen, die ansonsten nicht zugänglich sind.
		Frau Neufeld gab die entsprechenden Informationen dazu.
	</div>
	<div class="col-md-4">
		<img src="/assets/brueckentag_tag_der_offenen_tuer_15_fuehrung.jpg" alt="Brückentag - Führung" class="img-responsive center-block kastell-apotheke-bild"/>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<h2>Lagerautomat</h2>
	</div>
	<div class="col-md-4">
		<img src="/assets/brueckentag_tag_der_offenen_tuer_15_lagerautomat_vorstellung.jpg" alt="Brückentag - Lagerautomat" class="img-responsive center-block kastell-apotheke-bild"/>
	</div>
	<div class="col-md-8">
		Auf großes Interesse stieß der Lagerautomat, der vollautomatisch die Lagerhaltung und -pflege des Arzneimittelvorrats übernimmt.
		Herr Häfner zeigte den Automat in Aktion beim Ein- und Auslagern und erläuterte seine Arbeitswiese im Zusammenspiel mit der Apothekensoftware.
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<h2>Creme-Herstellung</h2>
	</div>
	<div class="col-md-8">
		Ein weiterer Kernbereich einer Apotheke ist die Rezeptur, in der individuell verordnete Medikamente hergestellt werden.
		Frau Hettinger erläuterte und demonstrierte praktisch die Herstellung einer Salbe.
		Solange der Vorrat reichte, durften die Besucher diese Handcreme mitnehmen.
	</div>
	<div class="col-md-4">
		<img src="/assets/brueckentag_tag_der_offenen_tuer_15_cremeherstellung.jpg" alt="Brückentag - Creme-Herstellung" class="img-responsive center-block kastell-apotheke-bild"/>
	</div>
</div>

## Abholstation
Ein besonderes Serviceangebot der Kastell-Apotheke ist die Abholstation.
Hier kann der Kunde einen Artikel, den die Apotheke ihm besorgt hat auch außerhalb der Öffnungszeiten abholen.
Er bekommt eine Abholnummer, mit der er seine Lieferung aus der Station auslagern und mitnehmen kann.
Um diesen Service einmal zu testen, wurden Abholnummern verteilt unter denen ein kleines Präsent hinterlegt war.

<div class="row">
	<div class="col-md-12">
		<h2>Glücksrad</h2>
	</div>
	<div class="col-md-4">
		<img src="/assets/brueckentag_tag_der_offenen_tuer_15_gluecksrad.jpg" alt="Brückentag - Glücksrad" class="img-responsive center-block kastell-apotheke-bild"/>
	</div>
	<div class="col-md-8">
		Das Glücksrad begeisterte Jung und Alt, denn jeder Besucher erhielt von Frau Gramlich einen Gewinn. Es bildete sich zeitweise eine lange Schlange.
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<h2>Magnesiumausschank</h2>
	</div>
	<div class="col-md-8">
		Als kleine Erfrischung wurde ein Magnesium-Mineralstoffgetränk ausgeschenkt.
		Und die Besucher konnten sich bei Frau Brenner über den Einsatz und die Wirkung von Magnesium ausführlich informieren.
	</div>
	<div class="col-md-4">
		<img src="/assets/brueckentag_tag_der_offenen_tuer_15_magnesiumausschank.jpg" alt="Brückentag - Magnesiumausschank" class="img-responsive center-block kastell-apotheke-bild"/>
	</div>
</div>

## tl;dr
Während des Osterburkener Brückenfestes präsentierte sich die Kastell-Apotheke in ihren neuen ansprechenden Räumlichkeiten und bot einen Blick hinter die Kulisse einer modernen Apotheke.
