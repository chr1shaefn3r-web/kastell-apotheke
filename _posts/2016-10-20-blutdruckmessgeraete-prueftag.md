---
title: Blutdruckmessgeräte-Prüftag
image: "blutdruckmessgeraete_prueftag_kastell_apotheke.jpg"
author: uh
layout: post
language: de
category: Neuigkeiten
---
In der Kastell-Apotheke fand am Donnerstag, den 20. Oktober 2016 der diesjährige Prüftag für Blutdruckmessgeräte statt.
Zum ersten Mal stand der Arbeitstisch für die Technikerin in der Offizin und interessierte Kunden informierten sich über den Ablauf der messtechnischen Kontrolle.
 
Nachdem seit einigen Jahren das Eichamt sein Monopol auf Nacheichung von Blutdruckmessgeräten aufgegeben hat, dürfen auch entsprehend qualifizierte Techniker das "Nacheichen" unter der neuen Bezeichnung "messtechnische Kontrolle" durchführen.
Gewerbliche Anwender wie z.B. Arztpraxen, Apotheken, Sozialstationen usw. sind verpflichtet, alle zwei Jahre ihre Blutdruckmessgeräte messtechnisch überprüfen zu lassen.
Für private Verwender gibt es diese Verpflichtung nicht.
Dennoch kann sich eine Privatperson mit dieser Überprüfung Gewissheit verschaffen, ob das häuslich genutzte Gerät noch die gesetzliche Genauigkeit besitzt.
 
Deshalb lädt die Kastell-Apotheke jedes Jahr im Oktober Frau Karin Hofer, Technikerin der Firma Uebe aus Wertheim ein, einen Tag lang für unsere Kunden deren Blutdruckmessgeräte messtechnisch zu überprüfen.
Mit der Firma Uebe verbindet uns seit Jahren eine Premium-Partnerschaft zum Nutzen unserer Kunden:

 * Wir bieten eine Verlängerung der Garantie von 3 auf 5 Jahre an.
 * Im Garantiefall müssen die Verbraucher nicht auf ein Einsenden und eine Überprüfung ihres Gerätes warten, sondern erhalten sofort ein Neugerät.
 * Durch günstigen Einkauf bieten wir faire Preise für hochwertige Geräte.
 * Wir bieten excellente Beratung und Tipps durch kontinuierliche Produktschulung.
 * Wir können einen Prüftag anbieten, und der Preis von 7,95 Euro pro Gerät ist im Vergleich zu ca. 30 Euro beim Eichamt sehr attraktiv.
 
Wir danken unseren Kunden für das rege Interesse am Aktionstag.
44 Geräte unterschiedlicher Marken hat Frau Hofer am Donnerstag geprüft.

