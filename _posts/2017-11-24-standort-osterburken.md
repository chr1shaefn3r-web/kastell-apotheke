---
title: Zukunft am Standort Osterburken sichern
image: "standort-osterburken_fn_17.jpg"
layout: post
author: mm
language: de
category: Neuigkeiten
---
<div class="kastell-apotheke-bild">
	<img src="/assets/fn-web-logo.png" class="img-responsive align-right twenty-percent-width" alt="FN Web Logo" />
	Dieser Artikel erschien urspr&uuml;nglich in den Fr&auml;nkischen Nachrichten am 24.&nbsp;November&nbsp;2017.
	&copy; Fränkische Nachrichten <a href="https://www.fnweb.de/fraenkische-nachrichten_artikel,-adelsheim-zukunft-am-standort-osterburken-sichern-_arid,1147310.html">Originalartikel</a>
</div>

Wenige Wochen, nachdem die sanierte Friedrichstraße für den Verkehr freigegeben wurde, gibt es schon wieder Neuigkeiten: Die dort angesiedelte „Kastell-Apotheke“ zieht um – und das schon 2018.

„Mit dem Umzug in das neue Gesundheitszentrum will ich die Zukunft meiner Apotheke am Standort Osterburken sichern“, so Ulla Häfner gestern Vormittag im Gespräch mit der FN. Vor nicht einmal drei Jahren, im Januar 2015, fand der letzte Ortswechsel statt. Damals war es nur „ein Katzensprung“ für das Team der „Kastell-Apotheke“: Von der Turmstraße 1 in das direkt gegenüber liegende Gebäude in der Friedrichstraße 12 (wir berichteten).

## Eröffnung für 20. August geplant

<img src="/assets/aerztehaus_fn_17.jpg" alt="Ärztehaus Osterburken" style="" class="img-responsive kastell-apotheke-bild align-right fourty-percent-width"/>

Dieses Mal – voraussichtlich wird es im August 2018 soweit sein – liegen ein paar Straßen „dazwischen“. Weit ist es aber nicht bis zum Osterburkener Fachmarktzentrum; zu Fuß sind es nur wenige Minuten. Wenn es so läuft, wie es sich die Apothekerin vorstellt, „wollen wir ab dem 6. August umziehen und am 20. August unsere Pforten erstmals am neuen Standort öffnen.“ Für Ulla Häfner ist es selbstverständlich, dass sie die Arbeitsplätze ihrer qualifizierten elf Mitarbeiterinnen auch in den neuen Räumen erhalten will – in denen die Kunden übrigens das „gewohnte Ambiente“ wiederfinden werden.

Ab Sommer 2018 wird die „Kastell-Apotheke“ also im neuen Gesundheitszentrum zu finden sein – und zwar im Erdgeschoss, ebenso wie eine Geschäftsstelle der „AOK“.

In weiteren Räumen des Gebäudes werden Praxen eröffnet – und zwar von Dr. Häußler (Zahnarzt) sowie von den Dres. Nafz/Schneider/Brümmer (Allgemeinärzte).

<div class="kastell-apotheke-bild">
	<img src="/assets/fn-web-logo.png" class="img-responsive align-right twenty-percent-width" alt="FN Web Logo" />
	Dieser Artikel erschien urspr&uuml;nglich in den Fr&auml;nkischen Nachrichten am 24.&nbsp;November&nbsp;2017.
	&copy; Fränkische Nachrichten <a href="https://www.fnweb.de/fraenkische-nachrichten_artikel,-adelsheim-zukunft-am-standort-osterburken-sichern-_arid,1147310.html">Originalartikel</a>
</div>
