---
title: Test your vein week
image: "Venen-Messwoche.png"
author: Kastell-Apotheke
layout: post
language: en
de: /2013/07/08/venen-messwoche.html
category: News
---
For a whole week long the Kastell-Apotheke offers free vein testing.

## Assignment of appointments

Please arrange your individual appointment via telephon 06291/68007 or visit us. The testing itself and the counseling interview last about half an hour. Functionality of the veins is tested at the shanks, therefore loose clothing is recommended.

## Method of measurement

The proven Lichtreflexionsrheographie will be used, a pain-less procedure to detect vein diseases via infrared light. As easy as checking blood pressure!

Further information on the topic of vein problems are provided [here][1] (German).

 [1]: /assets/Venen-Messwoche_13.pdf
