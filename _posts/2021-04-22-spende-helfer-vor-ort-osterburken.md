---
title: Spende an Helfer-vor-Ort-Gruppe des DRK-Ortsvereins Osterburken
image: "spende-helfer-vor-ort-osterburken_21.jpg"
layout: post
author: uh
language: de
category: Neuigkeiten
---
Helfer-vor-Ort-Gruppe des DRK-Ortsvereins Osterburken erhält großzügige Spende von der Kastell-Apotheke.

Im Rahmen der Aktion der Bundesregierung, vom 1. Januar bis zum 15. April 2021 jedem Bundesbürger über 60 Jahre oder mit Vorerkrankung jeweils 2 x 6 Schutzmasken mit hoher Schutzwirkung aus der Apotheke zur Verfügung zu stellen, spendete das Team der Kastell-Apotheke den Eigenanteil von jeweils 2 € an die Helfer-vor-Ort-Gruppe.
Am 22. April 2021 lud Frau Häfner zur symbolischen Scheckübergabe in Ihre Geschäftsräume.

Der stolze Betrag von <b>5.786,19 €</b> enthält neben den Eigenanteilen auch zahlreiche, spontane Spenden der Bevölkerung.
Dafür bedankte sich Frau Häfner herzlich bei Ihren Kunden.
Frau Häfner würdigte den segensreichen Dienst der ehrenamtlichen Einsatzkräfte vor Ort, die die Versorgung verunglückter Bürger bis zum Eintreffen der professionellen Rettungskräfte mit viel Engagement gewährleisten.

Im Namen des DRK bedankten sich Günter Albrecht (Vorsitzender), Sigrid Albrecht (HvO-Leitung), Klaus Rüdinger (Kassenwart) und Adelheid Kern (Einsatzleitung) für die außergewöhnliche Spende und freuten sich, dass ihre Arbeit so wertgeschätzt wird.
Da das Einsatzfahrzeug noch mit einem alten, analogen Funkgerät ausgestattet ist, fließt die Spende in die Neubeschaffung eines digitalen Geräts.
Frau Häfner dankte allen Anwesenden für Ihr Kommen, ihrem Team für die Unterstützung, ihrer Kundschaft für die Spendenbereitschaft und der Sparkasse Neckartal-Odenwald für Sparschwein und Scheck.
