---
title: New Homepage online
image: "homepage_relaunch.png"
author: Kastell-Apotheke
layout: post
language: en
de: /2013/06/17/neue-homepage-online.html
category: News
---
The new Homepage from the Kastell-Apotheke is online.

## Device independent

The new responsive design supplements our efforts in bringing the most readable and useful to our mobile visitors. If you decide to view our website rather in portrait then in landscape or vice versa, the new design will automatically detect the change and will change its view.

## What you should do

To get the most out of our new design, you should activate Javascript in your browser. If you are still using a very old version of your browser – go download the latest version. We are recommending this not only due to our new design but also to protect you from viruses and spyware.

## What we will do

With this new homepage we want to offer you even more digital goodies. Stay tunded!

