---
title: Neue Homepage online
image: "homepage_relaunch.png"
author: Kastell-Apotheke
layout: post
language: de
en: /2013/06/17/new-homepage-online.html
category: Neuigkeiten
---
Die neue Homepage der Kastell-Apotheke ist online. Dies bedeutet für Sie, dass Ihre Kastell-Apotheke jetzt noch präsenter im World-Wide-Web ist.

## Geräteunabhängig

Das neue anpassungsfähige Design ergänzt unsere Bemühungen, Ihnen unser Angebot auch mobil jederzeit und auf jedem Endgerät gut lesbar zu präsentieren. Wenn Sie sich entscheiden, die Inhalte mit Ihrem Smartphone oder Tablet lieber im Quer- als im Hochformat anzuschauen, ändert sich die Ansicht automatisch und wird für die jeweilige Bildschirm-Ausrichtung optimal dargestellt.

## Was Sie tun sollten

Um in den vollen Genuss des neuen Designs und seiner Funktionen zu kommen, sollten Sie in Ihrem Browser Javascript aktivieren. Falls Sie noch einen sehr alten Browser nutzen – laden Sie sich die aktuellste Version Ihres Browsers herunter. Dies empfehlen wir Ihnen nicht nur wegen des neuen Layouts, sondern auch zum Schutz vor Computer-Viren.

## Was wir tun wollen

Mit dieser neuen Homepage wollen wir Ihnen noch mehr digitale Angebote unterbreiten. Lassen Sie sich überraschen!

