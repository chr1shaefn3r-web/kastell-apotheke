---
title: Certificate awarded
image: "zl-ringversuch.jpg"
author: Kastell-Apotheke
layout: post
language: en
de: /2013/07/23/zertifikat-verliehen.html
category: News
---
## The Kastell-Apotheke successlly participated in the &#8220;ZL-Ringversuch&#8221; with additional certificate

To check and if needed improve the quality of our self-made medicine, the Kastell-Apotheke yearly participates in the &#8220;ZL-Ringversuch&#8221;. A central laboratory, run by the &#8220;Deutscher Apotheken e.V.&#8221; (national pharmacists society), offers this method of external quality assurance. The way this works is that the pharmacy has to send in a self-made medicine which is then tested on certain criteria. This year the medicine was a cortionse salve. Our salve was fully compliant to all tested criteria and therefore we were awarded the certificate.  
Our customers can be fully assured that they will receive high-quality and therefore effective medicine.  
To produce this high-quality pharmaceuticals time and care is needed. That is why we ask you, our customer, to wait patiently until the medicine is finished.

## tl;dr

The Kastell-Apotheke commits itself to always produce high quality medicine. This is tested on a regular base by an independent tester.

