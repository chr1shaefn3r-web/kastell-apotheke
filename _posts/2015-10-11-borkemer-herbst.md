---
title: Borkemer Herbst
image: "borkemer_herbst.jpg"
author: uh
layout: post
language: de
category: Neuigkeiten
---
Am Borkemer Herbst war während des verkaufsoffenen Sonntags
das Team der Krankengymnastik im Zentrum in den Räumen der Kastell-Apotheke zu Gast.
Inhaberin Frau Christine Weber und ihr Team boten kostenlose
Nacken- und Fußmassagen an und hatten dazu eine mobile Massagebank und einen
Massagestuhl sowie Informationsbroschüren mitgebracht.
Die Besucher des Borkemer Herbstes nahme das Angebot zu einer kleinen Auszeit
sehr gerne an.

Frau Häfner und der Gewerbeverein Osterburke bedanken sich herzlich bei Frau Weber und ihrem Team
für das "entspannnende" Angebot.
