---
title: Infos zu Pflegehilfsmitteln
image: "pflegetag.jpg"
author: uh
layout: post
language: de
category: Neuigkeiten
---
Sie versorgen einen pflegebedürftigen Angehörigen zu Hause, der in einen Pflegegrad (früher Pflegestufe) eingestuft ist?
Kennen Sie Ihren Anspruch auf kostenlose Versorgung mit Pflegehilfsmittel durch Ihre Pflegekasse?

Wir informieren Sie gerne!

Pflegebedürftige haben grundsätzlich Anspruch auf eine kostenlose Versorgung mit Pflegehilfsmittel zum Verbrauch, die zur Erleichterung der Pflege zu Hause beitragen (SGB XI §40 Abs.2). Voraussetzung ist:

 * die Einstufung in einen Pflegegrad (früher Pflegestufe)
 * die Pflege zu Hause durch einen Angehörigen ggf. auch mit Unterstüzung
 * eines professionellen Pflegedienstes
 * ein genehmigter Antrag auf Kostenübernahme der zuständigen Pflegekasse

Im Moment liegt die monatliche Pauschale bei 40 Euro und umfasst
folgende acht Pflegehilfsmittel:

 * saugende Bettschutzeinlagen zum Einmalgebrauch
 * Fingerlinge
 * Einmalhandschuhe
 * Mundschutz
 * Schutzschürzen zum Einmalgebrauch
 * wiederverwendbare Schutzschürzen
 * Händedesinfektionsmittell
 * Flächendesinfektionsmittel
 * waschbare saugende Bettschutzeinlagen

Wir stellen für Sie gerne den Antrag bei Ihrer Pflegekasse, kümmern uns um die Genehmigung und beraten Sie bei der Auswahl der Produkte, denn Hygiene und Schutz sind wichtige Aspekte bei der häuslichen Pflege.

Wir sind zugelassener Leistungserbringer bei allen Pflegekassen (gesetzlich und privat).

Nutzen Sie unseren wohnortnahen Service! Sprechen Sie uns vertrauensvoll an!
Wir helfen Ihnen gerne - unbürokratisch, persönlich, schnell!

Foto: © Africa Studio - Fotolia

