---
title: Zertifikat verliehen
image: "zl-ringversuch.jpg"
author: uh
layout: post
language: de
en: /2013/07/23/certificate-awarded.html
category: Neuigkeiten
---
## Erfolgreiche Teilnahme am ZL-Ringversuch mit Zertifikatserteilung

Um die Qualität der von uns in der Apotheke selbst angefertigten Rezepturarzneimittel z.B. Cremes, Salben, Lösungen usw. zu kontrollieren und wenn nötig zu verbessern, beteiligen wir uns jährlich am ZL-Ringversuch. Das Zentrallaboratorium Deutscher Apotheken e.V. bietet diese Möglichkeit zur externen Qualitätssicherung und fordert die Anfertigung einer praxisrelevanten Rezeptur, die dann nach vorgegebenen Parametern beurteilt wird.  
Dieses Jahr musste eine Cortisonsalbe hergestellt werden. Die von uns gefertigte Salbe entsprach in allen für die Zertifikatsvergabe relevanten Kriterien den vorgegebenen Anforderungen, und wir erhielten das begehrte Zertifikat.  
Unsere Kunden können sich also darauf verlassen, dass sie in der Kastell-Apotheke qualitativ hochwertige und somit wirksame Rezepturarzneimittel erhalten.  
Um solch qualitätsvolle Arzneimittel herzustellen, braucht es Sorgfalt und Zeit. Wir bitten Sie daher in Ihrem eigenen Interesse um Verständnis, dass Rezepturen eine angemessene Zeit zur Herstellung benötigen.

## tl;dr

Ihre Kastell-Apotheke verpflichtet sich, stets hochwertige Arzneimittel selbst herzustellen und lässt dies mit Erfolg regelmäßig von unabhängiger Stelle überprüfen.

