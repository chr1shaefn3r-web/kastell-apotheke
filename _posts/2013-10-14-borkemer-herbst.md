---
title: Borkemer Herbst
image: "borkemer_herbst_kastell_apotheke.jpg"
author: uh
layout: post
language: de
category: Neuigkeiten
---
Wie jedes Jahr beteiligte sich die Kastell-Apotheke am Borkemer Herbst mit einem verkaufsoffenem Sonntag.

## Almased-Beratungstag

In diesem Jahr veranstalteten wir einen Almased-Beratungstag mit Ausschank. All diejenigen, die ihren Stoffwechsel aktivieren und/oder gesund und nachhaltig Gewicht verlieren möchten, bekamen wertvolle Erläuterungen und Tipps. Neben ausführlichen Informationen konnten die Besucher verschiedene Almased-Drinks probieren, Broschüren mitnehmen, im Kochbuch blättern und einkaufen.  
Wiedereinmal bot die Kastell-Apotheke einen gelungenen Beratungstag an.

