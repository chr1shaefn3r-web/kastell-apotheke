---
title: Borkemer Herbst
image: "borkemer_herbst_kastell_apotheke_17.jpg"
author: uh
layout: post
language: de
category: Neuigkeiten
---
Anlässlich des Borkemer Herbstes am Sonntag, den 8. Oktober 2017 verkaufte die Kastell-Apotheke alte Glasgefäße und Teedosen.
***Der Erlös kommt der "Helfer-vor-Ort"-Gruppe des DRK Osterburken zu Gute.***

<div class="row">
	<div class="col-md-6">
		Margit Arnold und Axel Dressel stellten sich in den Dienst der guten Sache und boten am Sonntagnachmittag die Schätze aus der guten alten Apothekenzeit an.
		Viele Besucher des Borkemer Herbstes freuten sich über das seltene Angebot und suchten sich ihr Lieblingsstück aus.
		<br>
		Wer die gute Sache noch unterstützen und etwas aus dem Restbestand erwerben möchte, darf sich gerne bis Ende November an Frau Häfner wenden - bitte nach telefonischer Anmeldung.
	</div>
	<div class="col-md-6">
		<img src="/assets/borkemer_herbst_kastell_apotheke_17_teedosenverkauf.jpg" alt="Teedosen Verkauf Kastell-Apotheke" class="img-responsive kastell-apotheke-bild center-block"/>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<img src="/assets/borkemer_herbst_kastell_apotheke_17_eroeffnung.jpg" alt="Eroeffnung Friedrichstr" class="img-responsive kastell-apotheke-bild center-block"/>
	</div>
	<div class="col-md-6">
		<br><br>
		Im Rahmen des Borkemer Herbstes wurde die sanierte Friedrichsstraße (fast) wieder für den Verkehr freigegeben.
		Alle Beteiligten freuten sich über die Fertigstellung und durchschnitten feierlich das symbolische Band.
	</div>
</div>
