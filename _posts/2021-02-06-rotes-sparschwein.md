---
title: Rotes Sparschwein
image: "rotes-sparschwein_fn_21.jpg"
layout: post
author: nb
language: de
category: Neuigkeiten
---
<div class="kastell-apotheke-bild">
	<img src="/assets/fn-web-logo.png" class="img-responsive align-right twenty-percent-width" alt="FN Web Logo" />
	Dieser Artikel erschien urspr&uuml;nglich in den Fr&auml;nkischen Nachrichten am 06.&nbsp;Februar&nbsp;2021.
	&copy; Fränkische Nachrichten <a href="https://www.fnweb.de/fraenkische-nachrichten_artikel,-osterburken-bereits-1040-euro-gesammelt-_arid,1754353.html">Originalartikel</a>
</div>

Das große Sparschwein steht rund und maskiert auf einem „Heuhaufen“ in der Kastell-Apotheke.
Die Aktion „Schützen und unterstützen“, die von Ursula Häfner am 5. Januar ins Leben gerufen wurde, kommt gut bei ihren Kunden an.
Bereits 1040 Euro wurden beim Kauf von FFP2-Masken gespendet.
Schon vor einigen Tagen startete die zweite Phase der bundesweiten Schutzmasken-Abgabe an Risikopatienten.
Demnach haben besonders schutzbedürftige Personen ein Recht auf zwei mal sechs Masken, die sie in der Apotheke abholen können – unter Abgabe eines Berechtigungsscheins, den sie von ihrer privaten oder gesetzlichen Krankenkasse erhalten.
Bei Abgabe des Scheins beteiligt sich jeder Bezugsberechtigte mit zwei Euro am Maskenset.

## Masken „made in Germany“

In Osterburken werden FFP2-Masken mit höchsten Qualitätstandarts „made in Germany“ ausgegeben.
Darauf legt Ursula Häfner großen Wert, wie sie betont.
„Die Masken werden vom Hersteller ,univent’ regelmäßig auf ihre Filterqualität überprüft, das war uns sehr wichtig“, erklärt sie.
Da der Erstattungsbetrag für die Masken hoch genug ist, um sie zu finanzieren, wird die Eigenbeteiligung, die die Kunden zahlen müssen, stattdessen an die DRK-Ortsgruppe Osterburken gespendet.
So kamen in den ersten Wochen bereits rund 1000 Euro zusammen.
„Den neuesten Stand verkünden wir immer montags“, erklärt Häfner.
„Manche Kunden, die zu zweit kommen und eigentlich vier Euro für die Masken zahlen müssten, runden dann auch gerne mal auf fünf Euro auf.“
Die gesamte Aktion soll noch bis zum 15. April laufen.
So lange können die Berechtigungsscheine nämlich in der Apotheke abgegeben werden.
Wofür die DRK-Ortsgruppe das Geld verwendet, steht noch nicht fest.
Da die Mitglieder ihre Ausstattung jedoch selbst finanzieren, wird sich sicher etwas finden.
Und bis dahin hofft das Team der Kastell-Apotheke, dass noch viel Geld im roten Sparschwein landet.

<div class="kastell-apotheke-bild">
	<img src="/assets/fn-web-logo.png" class="img-responsive align-right twenty-percent-width" alt="FN Web Logo" />
	Dieser Artikel erschien urspr&uuml;nglich in den Fr&auml;nkischen Nachrichten am 06.&nbsp;Februar&nbsp;2021.
	&copy; Fränkische Nachrichten <a href="https://www.fnweb.de/fraenkische-nachrichten_artikel,-osterburken-bereits-1040-euro-gesammelt-_arid,1754353.html">Originalartikel</a>
</div>
