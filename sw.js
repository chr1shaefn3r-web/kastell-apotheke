importScripts('https://storage.googleapis.com/workbox-cdn/releases/6.0.2/workbox-sw.js');

if (workbox) {
    // This will trigger the importScripts() for workbox.strategies and its dependencies:
    workbox.loadModule('workbox-strategies');
    
    workbox.precaching.precacheAndRoute([],
        new workbox.strategies.NetworkFirst()
    );
}
