module.exports = {
  "globDirectory": "_build/",
  "globPatterns": [
    "**/kastell-apotheke.js",
    "**/*.webmanifest",
    "**/Logo-Kastell-Apotheke-Osterburken.png",
    "**/kastell-apotheke.jpg",
    "**/ankuendigungen/*.jpg",
    "**/favicon.ico"
  ],
  "swDest": "_build/sw.js",
  "swSrc": "sw.js"
};

